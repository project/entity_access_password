<?php

declare(strict_types=1);

namespace Drupal\entity_access_password\Service;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Provides a password access storage manager.
 */
class AccessStorageManager implements AccessStorageInterface {

  /**
   * The storage services.
   *
   * @var iterable
   */
  protected iterable $storages;

  /**
   * Constructor.
   *
   * @param iterable $storages
   *   The storage services.
   */
  public function __construct(iterable $storages) {
    $this->storages = $storages;
  }

  /**
   * {@inheritdoc}
   */
  public function storeEntityAccess(FieldableEntityInterface $entity): void {
    foreach ($this->storages as $storage) {
      $storage->storeEntityAccess($entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function storeEntityBundleAccess(FieldableEntityInterface $entity): void {
    foreach ($this->storages as $storage) {
      $storage->storeEntityBundleAccess($entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function storeGlobalAccess(): void {
    foreach ($this->storages as $storage) {
      $storage->storeGlobalAccess();
    }
  }

}
