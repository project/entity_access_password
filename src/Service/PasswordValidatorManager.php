<?php

declare(strict_types=1);

namespace Drupal\entity_access_password\Service;

use Drupal\entity_access_password\Plugin\Field\FieldType\EntityAccessPasswordItem;

/**
 * Provides a password validator manager.
 */
class PasswordValidatorManager implements PasswordValidatorInterface {

  /**
   * The validator services.
   *
   * @var iterable
   */
  protected iterable $validators;

  /**
   * Constructor.
   *
   * @param iterable $validators
   *   The validator services.
   */
  public function __construct(iterable $validators) {
    $this->validators = $validators;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePassword(string $password, EntityAccessPasswordItem $fieldItem): bool {
    foreach ($this->validators as $validator) {
      if ($validator->validatePassword($password, $fieldItem)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
