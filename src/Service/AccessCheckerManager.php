<?php

declare(strict_types=1);

namespace Drupal\entity_access_password\Service;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Provides a password access checker manager.
 */
class AccessCheckerManager implements AccessCheckerInterface {

  /**
   * The checker services.
   *
   * @var iterable
   */
  protected iterable $checkers;

  /**
   * Constructor.
   *
   * @param iterable $checkers
   *   The checker services.
   */
  public function __construct(iterable $checkers) {
    $this->checkers = $checkers;
  }

  /**
   * {@inheritdoc}
   */
  public function hasUserAccessToEntity(FieldableEntityInterface $entity): bool {
    foreach ($this->checkers as $checker) {
      // Stop on the first service granting access.
      if ($checker->hasUserAccessToEntity($entity)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasUserAccessToBundle(FieldableEntityInterface $entity): bool {
    foreach ($this->checkers as $checker) {
      // Stop on the first service granting access.
      if ($checker->hasUserAccessToBundle($entity)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasUserGlobalAccess(): bool {
    foreach ($this->checkers as $checker) {
      // Stop on the first service granting access.
      if ($checker->hasUserGlobalAccess()) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
